<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Loops in Forms: PHP Problem Set 2.1</title>
<link rel="stylesheet" type="text/css" href="problems.css" />
</head>

<body>
<h2>Loops in Forms: PHP Problem Set 2.1</h2>
    <form method="get">
   
<?php
    
    $months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    
        echo "<form>";
        echo "<select name='months'>";
        
    
    foreach($months as $month){
        echo "<option value='$month'>" . $month . "<option>";
    }
    
    echo "</select> ";
    
    echo "<select name='days'>";
    for($day = 1; $day<=31; $day++) {
        echo "<option value='$day'>$day<option>";
    }
    
    echo "</select> ";
    
    echo "<select name='years'>";
    for($year = 1900; $year <= 2016; $year++){
        echo "<option value='$year'>$year<option>";
    }
    
    echo "</select> ";
        $months = $_GET["months"];
            $days = $_GET["days"];
                $years = $_GET["years"];
    
   
    
     echo "{$months} {$days} {$years}";
     
	// Write a PHP program that displays user selectable numerical dates in 
	// a group of three select lists titled Month, Day and Year. The year 
	// should range from 1900 to the current year. On submit concatenate 
	// the date and display below the lists.
?>
       <input type="submit">
        
    </form>  
    
<!-- Hint: use loops! -->
<!-- Hint: See https://secure.php.net/manual/en/function.cal-days-in-month.php -->
</body>
</html>