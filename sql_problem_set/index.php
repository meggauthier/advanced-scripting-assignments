<!DOCKTYPE html>
<html>
<head>
<title>SQL Problems</title>
<link rel="stylesheet" type="text/css" href="problems.css" />
    <script type="text/javascript" src="../scripts/shCore.js"></script>
    <script type="text/javascript" src="../scripts/shBrushPhp.js"></script>
    <script type="text/javascript" src="../scripts/shBrushSql.js"></script>
    <link type="text/css" rel="stylesheet" href="../styles/shCoreDefault.css"/>
    <script type="text/javascript">SyntaxHighlighter.all();</script>
</head>
<body>
<h2>SQL Problem Set</h2>
<p>1. All problems within a "set" are due by the beginning of class on the next day that the class meets.<br />
  2. Each solution must be available via links on the OLS or comparable hosting and copied to the drop off drive. <br />
  3. Solutions to each problem set will be given during demonstrations on the day that they are due.<br />

  <a href="problem_set_3.1.php">Selecting Items: Problem Set 3.1</a><br />
  <a href="problem_set_3.2.php">Inserting Items: Problem Set 3.2</a><br />
  <a href="problem_set_3.3.php">Removing Items: Problem Set 3.3</a><br />
  <a href="problem_set_3.4.php">Updating Items: Problem Set 3.4</a><br />
  <a href="problem_set_3.5.php">Ordering Items: Problem Set 3.5</a></p>
  <pre class="brush: php;">
    $database  = new SQLite3("database.db");
    $query_string = "SELECT * FROM users";
    $result = $database->query($query_string);
    while ($row = $result->fetchArray())
    {
      echo "Id: {$row['id']} Name: {$row['name']}";
    }
    $database->close();
  </pre>
</body>
</html>