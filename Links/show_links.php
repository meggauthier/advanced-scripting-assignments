<!DOCKTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Bookmarks</title>
<link rel="stylesheet" type="text/css" href="styles.css" />
  <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
</head>

<body>
<h2>Stored Links</h2>
<table class="pure-table">
  <thead>
    <tr><td>Category</td><td>Link</td><td>Description</td><td>Action</td></tr>
  </thead>
<?php

  $query_string = "SELECT * FROM links;";
  
  // Open the database
  $database = new SQLite3("linksdatabase.db");  
  
  // Query the database
  $result = $database->query($query_string);
  echo "<tbody>";
  // Loop through results
  while ($row = $result->fetchArray())
  {
    echo "<tr>";
    echo "<td>{$row['category']}</td><td>{$row['link']}</td><td>{$row['description']}</td>";
      echo "<td><button onclick='removeLink({$row['id']})'>Remove</td>";
    echo "</tr>";
  }
  echo "</tbody>";
  // Close the database
  $database->close();
?>
</table>
</body>
</html>