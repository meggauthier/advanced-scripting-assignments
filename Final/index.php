<?php
  include('api/check_token.php');
?>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
      <style>
          body {
            background-color: #153d49;
            color: white;
            font-family: sans-serif;
            text-align: center;
          }
          p {
              color: white;
              font-size: 25px;
              padding-top: 20px;
          }
          a {
              font-family: sans-serif;
              color: white;
              transition: all .2s ease-in-out;
              border: solid white;
              border-radius: 7px;
              padding: 10px;
              margin-left: 15px;
              font-size: 20px;
              text-decoration: none;
          }
          a:hover {
              color: chartreuse;
              text-decoration: none;
              transform: scale(1.1);
          }
      
      
      </style>
    
   
  </head>
    
     <title>Time Study</title>
    
  <body>
    <p>You are logged in.</p><br>
      <br>
      <div id="links">
    
      <a href="form.php">Go to form</a>
    <a href="api/logout_user.php">Log out</a>
      </div>
          
  </body>
</html>