<?php
session_start(); 
?>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Time Study</title>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
 <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">

    <link rel="stylesheet" href="https://unpkg.com/purecss@0.6.0/build/grids-responsive-min.css">

   
  </head>
    <header>
        <h2> Please Login or Create Account</h2>
        <br>
        <br>
        <style> 
        body {
            background-image: url("MnTC_Logo_White.png");
            background-color: #153d49;
            background-repeat: no-repeat;
            background-position: center;
            border: 5px solid white;
            color: black;
            font-family: sans-serif;
            
            }
            h2 {
                text-align: center;
                font-family: sans-serif;
            }
            a {
                color: white;
                text-decoration: none;
            }
            a:hover {
                color: chartreuse;
                text-decoration: none;
            }
        
        </style>
    </header>
        
  <body>
    <div class="pure-g">
      <div class="pure-u-1 pure-u-md-1-6"></div>
      <div class="pure-u-1 pure-u-md-2-3">    
        <form id="uploadForm" action="api/login_user.php" method="post" enctype="multipart/form-data" class="pure-form pure-form-aligned">
          <div class="pure-control-group">
            <label for="name">Username</label>
            <input name="username" type="text" placeholder="Username" required>
          </div>
          <div class="pure-control-group">
            <label for="name">Password</label>
            <input name="password" type="password" placeholder="Password" required>
          </div>
          <div class="pure-controls">
            
            <input type="submit" class="pure-button pure-button-primary" value="Submit">
            <div id="response">
              <br><br>
              <?php
                if(isset($_SESSION["message"])) {
                  echo $_SESSION["message"];
                }
              ?>
            </div>
          </div>
        </form>
        
        <hr/>
        First time user? <a href="create.php">Register here</a>
      </div>
      <div class="pure-u-1 pure-u-md-1-6"></div>
    </div>

  </body>
</html>