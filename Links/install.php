<?php
    // Open the database (will create the file if it doesn't exist)
    $database = new SQLite3("linksdatabase.db"); 

    // Begin Create
    $query_string = "CREATE TABLE IF NOT EXISTS links (
                        id INTEGER PRIMARY KEY AUTOINCREMENT, 
                        time REAL DEFAULT CURRENT_TIMESTAMP,
                        category TEXT NOT NULL,
                        link TEXT NOT NULL,
                        description TEXT 
                      );";

    $database->query($query_string);
    echo "Database created";
    // End Create

    // Begin INSERT
    // Insert a test entry
    //$query_string = "INSERT INTO links (category, link, description)
                    // VALUES ('search', 'https://www.google.com', 'Google');";

    $database->query($query_string);
    echo "Insert complete";
    // End INSERT

    // Close the database
    $database->close();
?>