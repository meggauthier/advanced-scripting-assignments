<?php

$linkid = "";

if(isset($_GET['linkid'])) {
  $linkid = clean_input($_GET['linkid']);
    
    $query_string = "DELETE FROM links WHERE id = $linkid";
    
    $database = new SQLite3("linksdatabase.db");
    
    $database->query($query_string);
    
    $database->close();
    echo "Successfully deleted link $linkid";
} else {
    echo("Link ID is required");
}

function clean_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>