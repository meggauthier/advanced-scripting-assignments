<?php  
  session_start();
  $user_token = $_SESSION["user_token"];
  $user_id = $_SESSION["user_id"];
?>
<style>
    body {
         background-color: #153d49;
         color: white;
         font-family: sans-serif;
         text-align: center;
         font-size: 40px;
         padding-top: 125px;
    }
    #links {
        font-family: sans-serif;
        font-size: 30px;
    }
    a {
        font-size: 20px;
        color: white;
        text-decoration: none;
        border: solid white;
        border-radius: 5px;
        padding: 5px;
    }
    a:hover {
        height: 20px;
        width: 20px;
        text-decoration: none;
        color: chartreuse;
        transform: scale(20);
    }

</style>
<body>

<?php
    $position = "";
    $building = "";
    $week = "";
    $religion = "";
    $non_religion = "";
    $valid = true;


    $position = $_GET["position"];
    $building = $_GET["building"];
    $week = $_GET["week"];
    $religion = $_GET["religion"];
    $non_religion = $_GET["non_religion"];
  
    if ($valid == true) {
        $database = new mysqli("localhost", "root", "root", "time_study", 8889);         
        $dt = new DateTime();
        // Set the week / year
        $dt->setISODate(2016, intval($week)); // Update this to set the correct year.
        $dt->setTime(8,0);
        $query_string = "INSERT INTO entry (user_id, position, building, day, hours_r, hours_u) VALUES ('{$user_id}', '{$position}', '{$building}', '{$dt->format('U')}', '{$religion}', '{$non_religion}');";
      echo $query_string;
        $database->query($query_string);
        $database->close();
        echo "Success! ";
    } else {
        echo "Please fill in all fields. ";
    }

           
?>
</body>
    <div id="links">
    
      <a href="entry.php">View Entries</a>
    <a href="api/logout_user.php">Log out</a>
      </div>