###IM3420 - Advanced Scripting

This code will serve as a starting point for your landing page. All of the code changes you make must be checked in to version control (Bitbucket). Code must also be pushed to a web server prior to the due date for each assignment. We'll set up FTPloy during the first class to help simplify this process.

>Put each of your problem sets and projects in a sub folder within this directory. Each sub folder should have an index file associated to it. Link to each sub folder from your main index.php file.

###Git Commands
**Check-out**
1. cd ~/Desktop/
2. git clone YOUR_REPO_URL
3. cd IM3420

**Check-in**
1. git add -A
2. git commit -m "YOUR COMMIT MESSAGE"
3. git push -u origin master

###Syntax Highlighter
There are a number of CSS and JS files included in this project to provide syntax highlighting. Look at the Scratch Pad in the index.php for an example. This functionality allows you to display code on your website in a readable format. 
