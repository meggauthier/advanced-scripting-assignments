<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>IM3420 - Advanced Scripting</title>
    
    <!-- Look and feel of this page. Feel free to modify this! -->
    <link type="text/css" rel="stylesheet" href="styles.css"/>
    
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto" rel="stylesheet">
    
    <!-- These styles and JavaScript files are used to display code with syntax highlighting -->
    <script type="text/javascript" src="scripts/shCore.js"></script>
    <script type="text/javascript" src="scripts/shBrushPhp.js"></script>
    <script type="text/javascript" src="scripts/shBrushSql.js"></script>
    <link type="text/css" rel="stylesheet" href="styles/shCoreDefault.css"/>
    <script type="text/javascript">SyntaxHighlighter.all();</script>
    
  </head>
  <body>
    <div class="heading">
      <h1>Your Name</h1>
      IM3420
    </div>
    <div class="main-content">
      <div class="assignments">
        <h2>Problem Sets</h2>
        <ul>
          <li><a href="php_problem_set_1/">Problem Set 1</a></li>
          <li><a href="php_problem_set_2/">Problem Set 2</a></li>
          <li>Problem Set 3</li>
          <li>Problem Set 4</li>
          <li>Problem Set 5</li>
        </ul>
        <br/>
        <h2>Projects</h2>
        <ul>
          <li>Guestbook</li>
          <li>Links</li>
          <li>Final Project</li>
        </ul>
        <br/>
        <br/>
      </div>
      <div class="scratch-pad">
        <h2>Scratch Pad</h2>
        <pre class="brush: php;">
        // $ for variables
        $first_name = "John";
        
        // . to combine strings
        $last_name = "Smith";
        $full_name = $first_name . " " . $last_name;
        
        // echo to print to the screen
        echo $full_name;
        </pre>
      </div>
    </div>

  </body>
</html>