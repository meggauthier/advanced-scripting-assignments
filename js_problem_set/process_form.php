<?php
  $expected_inputs = ["one", "two", "three", "four", "five"];
  
  // https://secure.php.net/manual/en/language.variables.variable.php
  foreach($expected_inputs as $input) {
    if(isset($_POST[$input])) {
      $$input = filter_var($_GET[$input], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
    }
  }
  echo $one . "<br>";
  echo $two . "<br>";
  echo $three . "<br>";
  echo $four . "<br>";
  echo $five . "<br>";
?>