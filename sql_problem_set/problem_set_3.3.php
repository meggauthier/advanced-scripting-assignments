<!DOCKTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Deleting Items: SQL Problem Set 3.3</title>
<link rel="stylesheet" type="text/css" href="problems.css" />
  <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
</head>

<body>
<h2>Deleting Items: SQL Problem Set 3.3</h2>
  <?php
    // Reading: http://www.w3schools.com/sql/sql_delete.asp 
  
    // Open the database
    $database = new SQLite3("database.db"); 
    if(isset($_GET["treat"])) {
      $treat_id = $_GET["treat"];
      // Hint: You will need to use a DELETE statement
      $query_string = "DELETE FROM treats WHERE treatsid = $treat_id";
      $database->query($query_string);
    }
  ?>
  
<!-- Begin Form -->
<form class="pure-form" method="get" action="">
  <fieldset>
      <legend>Select an item to remove</legend>
      <label for="treats">Treats</label>
      <select id="treats" name="treat">
        <?php
          // Hint: You will need to use an SELECT statement
          $query_string = "SELECT * FROM treats;";
          
          // Query the database
          $result = $database->query($query_string);
          // Loop through results
          while ($row = $result->fetchArray())
          {
            echo "<option value=\"{$row['treatsid']}\">{$row['type']} ({$row['quantity']})</option>";
          }
        ?>
      </select>
      <button type="submit" class="pure-button pure-button-primary">Delete</button>
  </fieldset>
</form>
<!-- End Form -->

<!-- Code from problem set 3.1 to display results -->
<!-- Begin Table -->
<table class="pure-table">
  <thead>
    <tr><td>ID</td><td>Type</td><td>Quantity</td></tr>
  </thead>
  <?php
    // Rewind the results so we can use them again
    $result->reset();

    echo "<tbody>";
    // Loop through results
    while ($row = $result->fetchArray())
    {
      echo "<tr>";
      echo "<td>{$row['treatsid']}</td><td>{$row['type']}</td><td>{$row['quantity']}</td>";
      echo "</tr>";
    }
    echo "</tbody>";
    // Finally, close the database
    $database->close();
  ?>
</table>
<!-- End Table -->
</body>
</html>