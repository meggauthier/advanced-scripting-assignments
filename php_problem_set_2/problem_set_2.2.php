<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Reading a File into an Array: PHP Problem Set 2.2</title>
<link rel="stylesheet" type="text/css" href="problems.css" />
</head>

<body>
<h2>Reading a File into an Array: PHP Problem Set 2.2</h2>
<?php
    
    print_r(file("data.txt"));
    

    
	// Write a PHP program that reads in the contents of this file into an
	// array and displays it line by line preceded by line numbers.
?>
<!-- Hint: use the file function to create the array -->
<pre>
array ([0] => What is going on people?
       [1] => Testing, testing line 2.
       [2] => If this is printing to the third line then I am doing something right.
       [3] => I also did it on the same file page because the professor said so.
      )
<!-- Display the content here -->
</pre>
</body>
</html>