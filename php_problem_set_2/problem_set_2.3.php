<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Writing Content to a File: PHP Problem Set 2.3</title>
<link rel="stylesheet" type="text/css" href="problems.css" />
</head>

<body>
<h2>Writing Content to a File: PHP Problem Set 2.3</h2>
    <form action="" method="get">
        <input type="text" name="Name"> Name <br>
        <input type="text" name="Address"> Email <br>
        <input type="text" name="Phone"> Phone <br><br>
        <button>Submit</button>
    </form>
<?php
    
    $formdata1 = fopen("formfile.txt", "a") or die("Unable to open file!");
    
     $first = $_GET["Name"];
        $second = $_GET["Address"];
            $third = $_GET["Phone"];
    
    fwrite($formdata1,  "$first $second $third.\n");
    
    
    fclose($formdata1);
    
    echo " {$first}  {$second}  {$third}";

    
	// Write a PHP program that collects the input from a textarea, replaces
	// the new lines with break tags, and then adds the content to a line in
	// an external flat file. Display the contents of the flatfile 
	// below the form. 
?>

<!-- Hint: you will need to combine strings using the ".=" operator -->
<!-- Bonus: maintain the content entered by the user in the form as well -->
</body>
</html>