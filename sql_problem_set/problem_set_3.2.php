<!DOCKTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Inserting Items: SQL Problem Set 3.2</title>
<link rel="stylesheet" type="text/css" href="problems.css" />
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
</head>

<body>
<h2>Inserting Items: SQL Problem Set 3.2</h2>
  
  <!-- Step 1: Create an HTML form with two inputs type & quantity -->
  <form class="pure-form" method="post">
    <fieldset>
        <legend>A compact inline form</legend>

        <input type="text" name="type" placeholder="Type">
        <input type="text" name="quantity" placeholder="Quantity">

        <button type="submit" class="pure-button pure-button-primary">Add</button>
    </fieldset>
  </form>
<?php
  include "functions.php";
    
    
  // Write a PHP program that takes the content 
	// from an HTML form and writes the content to the
	// database.db SQLite file. Do not use more than 
	// one document (i.e. the "action" must be in the same script).
  
  // Step 2: Access and clean input variables
  if(isset($_POST["type"]) && isset($_POST["quantity"])) {
    $type = clean_input($_POST["type"]);
    $quantity = clean_input($_POST["quantity"]);
    $query_string = "INSERT INTO treats (type, quantity) VALUES ('$type', '$quantity')";
      
    // Step 3: Insert the data into the database
    // Hint: You will need to use an INSERT statement
    // Open the database
    $database = new SQLite3("database.db");  
    $database->query($query_string);
    $database->close();
  }
?>
    
<!-- Code from probelm set 3.1 to display the results -->
<table class="pure-table">
  <thead>
    <tr><td>ID</td><td>Type</td><td>Quantity</td></tr>
  </thead>
<?php
  // Reading: http://www.w3schools.com/sql/default.asp
  
	// Write a PHP program that displays all of the
  // entries from the database.db into an HTML table.
  
  // Hint: You will need to use an SELECT statement
  $query_string = "SELECT * FROM treats";
  
  // Open the database
  $database = new SQLite3("database.db");  
  
  // Query the database
  $result = $database->query($query_string);
  echo "<tbody>";
  // Loop through results
  while ($row = $result->fetchArray())
  {
    echo "<tr>";
    echo "<td>{$row['treatsid']}</td><td>{$row['type']}</td><td>{$row['quantity']}</td>";
    echo "</tr>";
  }
  echo "</tbody>";
  // Close the database
  $database->close();
?>
</table>
</body>
</html>

