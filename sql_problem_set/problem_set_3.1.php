<!DOCKTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Selecting Items: SQL Problem Set 3.1</title>
<link rel="stylesheet" type="text/css" href="problems.css" />
  <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
</head>

<body>
<h2>Selecting Items: SQL Problem Set 3.1</h2>
<table class="pure-table">
  <thead>
    <tr><td>ID</td><td>Type</td><td>Quantity</td></tr>
  </thead>
<?php
  // Reading: http://www.w3schools.com/sql/default.asp
  
	// Write a PHP program that displays all of the
  // entries from the database.db into an HTML table.
  
  // Hint: You will need to use an SELECT statement
  $query_string = "SELECT * FROM treats;";
  
  // Open the database
  $database = new SQLite3("database.db");  
  
  // Query the database
  $result = $database->query($query_string);
  echo "<tbody>";
  // Loop through results
  while ($row = $result->fetchArray())
  {
    echo "<tr>";
    echo "<td>{$row['treatsid']}</td><td>{$row['type']}</td><td>{$row['quantity']}</td>";
    echo "</tr>";
  }
  echo "</tbody>";
  // Close the database
  $database->close();
?>
</table>
</body>
</html>