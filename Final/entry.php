<!DOCTYPE html>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    
    <title> Entries</title>
    </head>
    
    <body>
    <h2> Past Entries</h2>
        
     <table>
        <thead>
        <tr>
            <td> Position</td>
            <td> Building</td>
            <td> Week</td>
            <td> Religious</td>
            <td> Non-Religious</td>
        </tr>
        </thead>
<?php
        $query_string = "SELECT * FROM entry;";
            
        $database = new mysqli("localhost", "root", "root", "time_study", 8889); 
        
        $result = $database->query($query_string);
        echo "<tbody>";
        
        while ($row = $result->fetch_array())
        {
            echo "<tr>";
            echo "<td>{$row['position']}</td><td>{$row['building']}</td><td>{$row['day']}</td><td>{$row['hours_r']}</td><td>{$row['hours_u']}</td>";
            echo "</tr>";
        }
        echo "</tbody>";
        
        $database->close();
        
    ?>
        </table>  
    </body>

</html>