<!DOCKTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Updating Items: SQL Problem Set 3.4</title>
<link rel="stylesheet" type="text/css" href="problems.css" />
  <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
</head>

<body>
<h2>Updating Items: SQL Problem Set 3.4</h2>
<?php
  // Reading: http://www.w3schools.com/sql/sql_update.asp
  
	// Write a PHP program that takes the content 
	// from an HTML form and updates the content in the
	// database.db SQLite file. Do not use more than 
	// one document (i.e. the "action" must be in the same script).
    
  // Open the database
  $database = new SQLite3("database.db"); 
  if(isset($_GET["treat"])) {
    $treat_id = $_GET["treat"];
    // Hint: You will need to use an UPDATE statement
    $query_string = "UPDATE treats SET quantity = quantity - 1 WHERE treatsid = $treat_id";
    $database->query($query_string);
  }
  
  // SELECT statement from problem 3.1 goes here
  $query_string = "SELECT * FROM treats";

  // Query the database
  $result = $database->query($query_string);
  while ($row = $result->fetchArray())
  {
    echo "<form class='pure-form' method='get' action=''>";
    echo "<input type='hidden' name='treat' value='{$row['treatsid']}'>";
    echo "<input type='submit' value='Consume {$row['type']}'>";
    echo "</form>";
  }
?>
  
<!-- Code from problem set 3.1 to display results -->
<!-- Begin Table -->
<table class="pure-table">
  <thead>
    <tr><td>ID</td><td>Type</td><td>Quantity</td></tr>
  </thead>
  <?php
    // Rewind the results so we can use them again
    $result->reset();
  
    echo "<tbody>";
    // Loop through results
    while ($row = $result->fetchArray())
    {
      echo "<tr>";
      echo "<td>{$row['treatsid']}</td><td>{$row['type']}</td><td>{$row['quantity']}</td>";
      echo "</tr>";
    }
    echo "</tbody>";
    // Finally, close the database
    $database->close();
  ?>
</table>
<!-- End Table -->
</body>
</html>